import { Component, ElementRef, ViewChild } from '@angular/core';
import { VentasServiceService } from '../ventas-service.service';
import * as d3 from 'd3';

@Component({
  selector: 'app-ventas',
  templateUrl: './ventas.component.html',
  styleUrls: ['./ventas.component.css']
})
export class VentasComponent {
  ventas: any = [];
  ventasAnio: any = [];
  totalAnual: any = [];
  gVentas: any = [];
  sucursal: string = '';
  mes: number = 1;
  monto: number | null = null;
  sucursalSeleccionada: string = '';
  mesDesde: number = 1;
  mesHasta: number = 1;
  resultadoConsulta: any;
  totalVentas: number = 1;
  errorConsulta: string | null = null;

  edicionEnProgreso: boolean = false;
  valorEditado: number = 0;
  indice: number = 0;
  filaEditada: any; // Mantén un registro de la fila y la columna editadas.

  operacion: string = '';

  constructor(private ventasServiceService: VentasServiceService) { }

  
  ngOnInit(): void {
    this.ventasServiceService.obtenerVentas().subscribe((data): void => {
      this.ventas = data;
      /* alert(JSON.stringify(data)); */
    });
    this.ventasServiceService.obtenerVentasxAnio().subscribe((data): void => {
      this.ventasAnio = data;
    });
    this.ventasServiceService.obtenerTotalAnual().subscribe((data): void => {
      this.totalAnual = data;
    }) 
  }

  getSucursales(ventasData: any): any[] {
    const sucursales = [];
    for (const key in ventasData) {
      if (ventasData.hasOwnProperty(key)) {
        sucursales.push({ nombre: key, total: ventasData[key] });
      }
    }
    return sucursales;
  }

  getTotal(ventasData: any): any[]{
    const total = [];
    for(const key in ventasData) {
      if (ventasData.hasOwnProperty(key)) {
        total.push({ total: ventasData[key] });
      }
    }
    return total;
  }

  

  consultarVentas() {
    if(this.mesHasta >= this.mesDesde) {
      this.ventasServiceService.obtenerVentasxMeses(this.sucursalSeleccionada, this.mesDesde, this.mesHasta).subscribe(
        (total: any) => {
          if(total.length > 0) {
            this.resultadoConsulta = total[0].total_ventas
            this.errorConsulta = null; // Limpia cualquier error anterior
          } else {
            this.resultadoConsulta = null;
          }
        },
        (error) => {
          console.error('Error al consultar las ventas', error);
          this.errorConsulta = 'Error al consultar las ventas. Por favor, inténtelo de nuevo.';
        }
      );
    } else {
      console.error('Error: El mesHasta no puede ser menor que el mesDesde');
      this.errorConsulta = 'Error: El mes hasta no puede ser menor que el mes desde';
    }
  }

  getNombreMes(numeroMes: number): string {
    const meses = [
      'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
      'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'
    ];
    return meses[numeroMes - 1];
  }

  recuperarVentas(): void {
    this.ventasServiceService.obtenerVentas().subscribe((data): void => {
      this.ventas = data;      
    });
  }

  insertarVenta(){
    this.ventasServiceService.insertarVenta(this.mes, this.sucursal, this.monto ?? 0, this.operacion).subscribe((resultado) => {
      console.log("Venta ingresada con exito.", resultado);
      this.obtenerVentas();
      this.obtenerPorSucursal();
      this.obtenerPorAnio();

      this.mes = 0;
      this.sucursal = '';
      this.monto = 1;
    },
    (error) => {
      console.error("Error al insertar el cliente.", error);
    }
    );
  }

  obtenerVentas(): void {
    this.ventasServiceService.obtenerVentas().subscribe((data) => {
      this.ventas = data;
    });
  }

  obtenerPorSucursal(): void {
    this.ventasServiceService.obtenerVentasxAnio().subscribe((data): void => {
      this.ventasAnio = data;
    });
  }

  obtenerPorAnio(): void {
    this.ventasServiceService.obtenerTotalAnual().subscribe((data): void => {
      this.totalAnual = data;
    });
  }

  editarValor(valor: number, fila: any, indice: number): void {
    this.edicionEnProgreso = true;
    this.valorEditado = valor;
    this.filaEditada = fila;
  }

  guardarEdicion(): void {
    // Realiza la lógica para guardar la edición en la base de datos.
    // Luego, establece edicionEnProgreso en false.
    this.edicionEnProgreso = false;
  
    // Actualiza la tabla de totales llamando a una función que obtiene los totales actualizados.
    this.obtenerPorSucursal();
    this.obtenerPorAnio();
  }
  
  cancelarEdicion(): void {
    // Restaura el valor original y cancela la edición.
    this.filaEditada.SF = this.valorEditado; // Esto puede variar según tu estructura de datos.
    this.edicionEnProgreso = false;
  }

}