import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Venta } from './venta';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class VentasServiceService {
  private baseUrl = 'http://localhost/php/ventas/';

  constructor(private http: HttpClient) { };

  obtenerVentas(): Observable<any[]> {
    const url = `${this.baseUrl}findAllVentas.php`;
    return this.http.get<any[]>(url);
  }

  obtenerVentasxAnio(): Observable<any[]> {
    const url = `${this.baseUrl}ventasAnio.php`;
    return this.http.get<any[]>(url);
  }

  obtenerTotalAnual(): Observable<any[]> {
    const url = `${this.baseUrl}ventaAnual.php`;
    return this.http.get<any[]>(url);
  }

  obtenerVentasxMeses(sucursal: string, mesDesde: number, mesHasta: number): Observable<{ total_ventas: number }> {
    const url = `${this.baseUrl}findVenta.php`;
    const params = {
      sucursal: sucursal,
      mesDesde: mesDesde,
      mesHasta: mesHasta
    };
    return this.http.get<{ total_ventas: number }>(url, { params: params});
  }

  insertarVenta(mes: number, sucursal: string, monto: number, operacion: string): Observable<any> {
    const url = `${this.baseUrl}insertVenta.php`;
    const data = {mes, sucursal, monto, operacion};
    return this.http.post(url, data);
  }
}
