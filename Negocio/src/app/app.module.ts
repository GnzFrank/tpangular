import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { ListaClientesComponent } from './lista-clientes/lista-clientes.component';
import { ListaSucursalesComponent } from './lista-sucursales/lista-sucursales.component';
import { VentasComponent } from './ventas/ventas.component';

import { HttpClientModule } from '@angular/common/http';
import { HeaderComponent } from './header/header.component';
import { InsertClienteComponent } from './insert-cliente/insert-cliente.component';
import { ActualizarClienteComponent } from './lista-clientes/actualizar-cliente/actualizar-cliente.component';
import { GraficosComponent } from './ventas/graficos/graficos.component';
import { FooterComponent } from './footer/footer.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ListaClientesComponent,
    ListaSucursalesComponent,
    VentasComponent,
    HeaderComponent,
    InsertClienteComponent,
    ActualizarClienteComponent,
    GraficosComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
