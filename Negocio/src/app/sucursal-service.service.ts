import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SucursalServiceService {
  private baseUrl = 'http://localhost/php/sucursales/';


  constructor(private http: HttpClient) { };

  obtenerSucursales(): Observable<any[]> {
    const url = `${this.baseUrl}findSucursales.php`;
    return this.http.get<any[]>(url);
  }
}
