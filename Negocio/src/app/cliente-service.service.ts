import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Clientes } from './clientes';
import { Observable, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ClienteServiceService {

  private baseUrl = 'http://localhost/php/cliente/';

  // Utiliza un BehaviorSubject para almacenar y emitir la lista de clientes
  private clientesSubject = new BehaviorSubject<any[]>([]);

  constructor(private http: HttpClient) { };

  // Agregar una función para obtener y emitir la lista de clientes
  obtenerYActualizarClientes() {
    this.obtenerClientes().subscribe((clientes) => {
      this.clientesSubject.next(clientes);
    });
  }

  // Obtener un observable para que los componentes se suscriban a la lista de clientes
  obtenerClientesObservable(): Observable<any[]> {
    return this.clientesSubject.asObservable();
  }

  obtenerClientes(): Observable<any[]> {
    const url = `${this.baseUrl}findAllCliente.php`;
    return this.http.get<any[]>(url);
  }

  cambiarStatus(clienteId: number, clienteStatus: number): Observable<any>{
    const url = `${this.baseUrl}cambiarStatus.php`;
    if(clienteStatus == 0){
      clienteStatus = 1;
    }else{
      clienteStatus = 0;
    }
    /* clienteStatus= ((clienteStatus +1)%2); */
    const body = { id: clienteId, status: clienteStatus };
    console.log(clienteId, clienteStatus);
    return this.http.post(url, body);
  }

  eliminarCliente(clienteId: number): Observable<any>{
    const url = `${this.baseUrl}deleteCliente.php`;
    const body = { id: clienteId };
    return this.http.post(url, body);
  }

  insertarClientes(cliente: Clientes): Observable<any> {
    const url = `${this.baseUrl}insertCliente.php`;
    return this.http.post(url, cliente);
  }

  

  actualizarCliente(cliente: Clientes, httpOptions: any): Observable<any> {
    const url = `${this.baseUrl}updateCliente.php`;
    return this.http.post(url, cliente, httpOptions);
  }
  
  obtenerClientePorId(clienteId: number): Observable<any>{
    const url = `${this.baseUrl}findCliente.php`;
    const body = { id: clienteId };

    console.log('Solicitud a URL:', url); // Agregar este console.log para verificar la URL
    console.log('Cuerpo de la solicitud:', body); // Agregar este console.log para verificar los datos enviados

    return this.http.post(url, body);
  }
}
