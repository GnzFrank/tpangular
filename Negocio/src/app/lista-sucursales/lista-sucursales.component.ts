import { Component } from '@angular/core';
import { SucursalServiceService } from '../sucursal-service.service';

@Component({
  selector: 'app-lista-sucursales',
  templateUrl: './lista-sucursales.component.html',
  styleUrls: ['./lista-sucursales.component.css']
})
export class ListaSucursalesComponent {
  sucursales: any =[];

  constructor(private sucursalesService: SucursalServiceService) {}

  ngOnInit(): void {
    this.sucursalesService.obtenerSucursales().subscribe((data): void => {
      this.sucursales = data;
      /* alert(JSON.stringify(data)); */
    });
  }
}

