export class Venta {
    public mes: number;
    public sucursal: String;
    public monto: number;

    constructor(
        mes: number = 0,
        sucursal: String = '',
        monto: number = 0
    ){
        this.mes = mes;
        this.sucursal = sucursal;
        this.monto = monto;
    }
}