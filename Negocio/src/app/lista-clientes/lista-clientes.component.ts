import { Component } from '@angular/core';
import { ClienteServiceService } from '../cliente-service.service';
import { ElementRef } from '@angular/core';

@Component({
  selector: 'app-lista-clientes',
  templateUrl: './lista-clientes.component.html',
  styleUrls: ['./lista-clientes.component.css']
})
export class ListaClientesComponent{
  clientes: any = [];
  selectedCliente: any = null;

  constructor(private clienteServiceService: ClienteServiceService, private el: ElementRef) {}

  ngOnInit(): void {
    // Llama a la función para obtener y actualizar clientes al iniciar el componente
    this.clienteServiceService.obtenerYActualizarClientes();
    this.clienteServiceService.obtenerClientesObservable().subscribe((clientes) => {
      this.clientes = clientes;
    });
  }

  cambiarStatus(clienteId: number, clienteStatus: number): void {
    this.clienteServiceService.cambiarStatus(clienteId, clienteStatus).subscribe(() => {
      this.actualizarListaClientes();
    });
  }

  eliminarCliente(clienteId: number): void {
    this.clienteServiceService.eliminarCliente(clienteId).subscribe(() => {
      this.actualizarListaClientes();
    });
  }

  actualizarListaClientes(): void {
    this.clienteServiceService.obtenerClientes().subscribe((data):void => {
      this.clientes = data;
    });
  }

  getColorBackground(status: number): any {
    if (status == 1) {
      return { 'background-color': 'green' };
    } else {
      return { 'background-color': 'red' }
    }
  }
}
