import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ClienteServiceService } from '../../cliente-service.service';
import { Clientes } from '../../clientes';
import { HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-actualizar-cliente',
  templateUrl: './actualizar-cliente.component.html',
  styleUrls: ['./actualizar-cliente.component.css']
})
export class ActualizarClienteComponent {
  
  cliente: Clientes = new Clientes();
  
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    })
  };

  constructor(private route: ActivatedRoute, private router: Router, private clienteServiceService: ClienteServiceService ) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      const clienteIdParam = params.get('id');
      if (clienteIdParam !== null) {
        const clienteId = parseInt(clienteIdParam, 10);
        if (!isNaN(clienteId)) {
          const body = { id: clienteId };
          this.clienteServiceService.obtenerClientePorId(clienteId).subscribe(cliente => {
            this.cliente.id = clienteId;
            //console.log('Datos del cliente obtenidos:', this.cliente.id); // Agregar este console.log para verificar la respuesta
          });
        } else {
          console.error("ID de cliente no válido.");
        }
      } else {
        console.error("ID de cliente no proporcionado.");
      }
    });
  }

  onSubmit() {
    this.clienteServiceService.actualizarCliente(this.cliente, this.httpOptions).subscribe(
      (response) => {
        console.log("Cliente actualizado con éxito.", response);
        this.router.navigate(['/clientes']); // Después de la actualización, vuelve a la lista de clientes
      },
      (error) => {
        console.error("Error al actualizar el cliente.", error);
      }
    );
  }
}
