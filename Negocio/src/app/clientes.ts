export class Clientes {
    public id: number;
    public apellido: String;
    public nombre: String;
    public saldo: number;
    public status: number;
    
    constructor(
        id: number = 0,
        apellido: string = '',
        nombre: String = '',
        saldo: number = 0,
        status: number = 1
    ){
        this.id = id;
        this.apellido = apellido;
        this.nombre = nombre;
        this.saldo = saldo;
        this.status = status;
    }
}
