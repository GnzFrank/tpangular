import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import {HomeComponent} from './home/home.component';
import {ListaClientesComponent} from './lista-clientes/lista-clientes.component';
import { ListaSucursalesComponent } from './lista-sucursales/lista-sucursales.component';
import { VentasComponent } from './ventas/ventas.component';
import { InsertClienteComponent } from './insert-cliente/insert-cliente.component';
import { ActualizarClienteComponent } from './lista-clientes/actualizar-cliente/actualizar-cliente.component';
import { GraficosComponent } from './ventas/graficos/graficos.component';

const routes: Routes = [
  { path: 'sucursales', component: ListaSucursalesComponent },
  { path: 'clientes', component: ListaClientesComponent },
  { path: 'inicio', component: HomeComponent },
  { path: '', redirectTo: '/inicio', pathMatch: 'full' },
  { path: 'ventas', component: VentasComponent },
  { path: 'insertCliente/:id/:apellido/:nombre/:saldo/:status', component: InsertClienteComponent },
  { path: '', redirectTo: '/clientes', pathMatch: 'full' },
  { path: 'actualizarCliente/:id', component: ActualizarClienteComponent },
  { path: 'graficos', component: GraficosComponent },
  ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
