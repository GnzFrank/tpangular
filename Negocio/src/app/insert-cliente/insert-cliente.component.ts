import { Component, ViewChild } from '@angular/core';
import { ClienteServiceService } from '../cliente-service.service';
import { Clientes } from '../clientes';
import { ActivatedRoute, Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { NgModel, NgModelGroup } from '@angular/forms';

@Component({
  selector: 'app-insert-cliente',
  templateUrl: './insert-cliente.component.html',
  styleUrls: ['./insert-cliente.component.css']
})
export class InsertClienteComponent {
  cliente: Clientes = new Clientes();
  clientes: any = [];

  constructor(private route: ActivatedRoute, private router: Router, private clienteServiceService: ClienteServiceService){}

  @ViewChild('insertForm') insertForm!: NgForm;

  onSubmit(){
    this.clienteServiceService.insertarClientes(this.cliente).subscribe((response) => {
      console.log("Cliente ingresado con exito.", response);
      //esto llama
      this.clienteServiceService.obtenerYActualizarClientes();
      this.cliente.apellido = '';
      this.cliente.nombre = '';
    },
    (error) => {
      console.error("Error al insertar el cliente.", error);
    }
    );
  }
}
