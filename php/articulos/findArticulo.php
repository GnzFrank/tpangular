<?php
include('../db.php'); // Incluye el archivo de conexión a la base de datos

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $id = $_POST["id"];

    // Realiza la consulta para buscar al cliente por ID
    $sql = "SELECT * FROM articulos WHERE id = '$id'";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        // Se encontró al menos un cliente con el ID proporcionado
        $row = $result->fetch_assoc();
        echo "ID: " . $row["id"] . "<br>";
        echo "Descripcion: " . ($row["descripcion"] ?? '') . "<br>";
        echo "Precio: " . ($row["precio"] ?? '') . "<br>";
    } else {
        echo "No se encontró ningún articulo con ese ID.";
    }
}

$conn->close();
?>