<?php
session_start(); 
header("Access-Control-Allow-Origin: *"); 
header("Access-Control-Allow-Methods: PUT, GET, POST, DELETE");
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");

include('../db.php');

if ($_SERVER["REQUEST_METHOD"] === "POST") {
    // Obtiene los datos del cliente desde el cuerpo de la solicitud
    $data = json_decode(file_get_contents("php://input"));

    // Verifica si se recibieron los datos requeridos
    if (isset($data->apellido) &&
        isset($data->nombre) &&
        isset($data->saldo) &&
        isset($data->status)) {
            $apellido = $data->apellido;
            $nombre = $data->nombre;
            $saldo = $data->saldo;
            $status = $data->status;

        $sql = "INSERT INTO clientes (apellido, nombre, saldo, status) VALUES ('$apellido', '$nombre', '$saldo', '$status')";

        if ($conn->query($sql) === TRUE) {
            echo json_encode(["message" => "Cliente insertado correctamente"]);
        } else {
            echo json_encode(["error" => "Error al insertar el cliente: " . $conn->error]);
        }
    } else {
        echo json_encode(["error" => "Datos incompletos en la solicitud"]);
    }
} else {
    echo json_encode(["error" => "Método no permitido"]);
}

$conn->close();
?>