<?php
session_start(); 
header("Access-Control-Allow-Origin: *"); 
header("Access-Control-Allow-Methods: PUT, GET, POST, DELETE");
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
include('../db.php');

if ($_SERVER["REQUEST_METHOD"] === "POST") {
    $data = json_decode(file_get_contents("php://input")); // Leer datos JSON del cuerpo de la solicitud
    if (isset($data->id)) {
        $id = $data->id;

        $sql = "DELETE FROM clientes WHERE id='$id'";
        
        if ($conn->query($sql) === TRUE) {
            echo json_encode(["message" => "Cliente eliminado con éxito"]);
        } else {
            echo json_encode(["error" => "Error al eliminar el cliente: " . $conn->error]);
        }
    } else {
        echo json_encode(["error" => "El parámetro 'id' es obligatorio"]);
    }
} else {
    echo json_encode(["error" => "Método no permitido"]);
}

$conn->close();
?>
