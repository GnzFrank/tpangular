<?php
session_start(); 
header("Access-Control-Allow-Origin: *"); 
header("Access-Control-Allow-Methods: PUT, GET, POST, DELETE");
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
include('../db.php');

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    
    $data = json_decode(file_get_contents("php://input"));

    if (isset($data->id) &&
        isset($data->apellido) &&
        isset($data->nombre) &&
        isset($data->saldo) &&
        isset($data->status)) {
            $id = $data->id;
            $apellido = $data->apellido;
            $nombre = $data->nombre;
            $saldo = $data->saldo;
            $status = $data->status;

        $sql = "UPDATE clientes SET apellido='$apellido', nombre='$nombre', saldo='$saldo', status='$status' WHERE id='$id'";
            
        if ($conn->query($sql) === TRUE) {
            echo json_encode(["message" => "Cliente actualizado correctamente"]);
        } else {
            echo json_encode(["error" => "Error al actualizar el cliente: " . $conn->error]);
        }
    } else {
        echo json_encode(["error" => "Datos incompletos en la solicitud"]);
    }
} else {
    echo json_encode(["error" => "Método no permitido"]);
}

$conn->close();
?>
