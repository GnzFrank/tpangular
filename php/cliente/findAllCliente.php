<?php
session_start(); 
header("Access-Control-Allow-Origin: *"); 
header("Access-Control-Allow-Methods: PUT, GET, POST, DELETE");
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");

include('../db.php'); // Incluye el archivo de conexión a la base de datos

// Realiza la consulta para obtener todas las sucursales
$sql = "SELECT * FROM clientes";
$vec = [];
$result = $conn->query($sql);

/* $vec [] = $result; */


if ($result->num_rows > 0) {
    
    while ($row = $result->fetch_assoc()) {
        /* echo "ID: " . $row["id"] . "<br>"; */

        $vec[] = $row;

        /* echo "Apellido: " . $row["Apellido"] . '<br>';
        echo "Nombre: " . $row["Nombre"] . "<br>";
        echo "Saldo: " . $row["Saldo"] . "<br>";
        echo "Status: " . $row["Status"] . "<br>";
        echo "<br>"; */ }
} else {
    echo "No se encontraron sucursales en la base de datos.";
}
echo json_encode($vec);
$conn->close();
?>