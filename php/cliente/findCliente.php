<?php
session_start();
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: PUT, GET, POST, DELETE");
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
include('../db.php');

if ($_SERVER["REQUEST_METHOD"] === "POST") {
    if (isset($_POST["id"])) { // Verificar si "id" está definido en $_POST
        $id = $_POST["id"];

        var_dump($_POST);

        $sql = "SELECT * FROM clientes WHERE id = '$id'";

        $result = $conn->query($sql);

        if ($result && $result->num_rows > 0) {
            $cliente = $result->fetch_assoc();
            echo json_encode(["cliente" => $cliente]);
        } else {
            echo json_encode(["error" => "Cliente no encontrado"]);
        }
    } else {
        echo json_encode(["error" => "ID de cliente no proporcionado"]);
    }
} else {
    echo json_encode(["error" => "Método no permitido"]);
}

$conn->close();
?>
