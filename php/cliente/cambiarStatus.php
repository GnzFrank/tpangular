<?php
session_start(); 
header("Access-Control-Allow-Origin: *"); 
header("Access-Control-Allow-Methods: PUT, GET, POST, DELETE");
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
include('../db.php');

if ($_SERVER["REQUEST_METHOD"] === "POST") {
    $data = json_decode(file_get_contents("php://input")); // Leer datos JSON del cuerpo de la solicitud
    if (isset($data->id) &&
        isset($data->status)) {
        $id = $data->id;
        $status = $data->status;
        
        $sql = "UPDATE clientes
                SET status = $status /* IF ($status == 1, 0, 1) este no funciona porque algo del IF no lo toma bien*/ 
                WHERE id='$id'";
        
        //con esto de abajo se puede debugear el php y te crea un archivo hermano a donde este el archivo.
        /* $file = fopen("caca.dat", "w");
        fwrite ($file, "consulta= ". $sql);
        fclose ($file); */

        if ($conn->query($sql) === TRUE) {
            echo json_encode(["message" => "Cliente eliminado con éxito"]);
        } else {
            echo json_encode(["error" => "Error al eliminar el cliente: " . $conn->error]);
        }
    } else {
        echo json_encode(["error" => "El parámetro 'id' es obligatorio"]);
    }
} else {
    echo json_encode(["error" => "Método no permitido"]);
}

$conn->close();
?>