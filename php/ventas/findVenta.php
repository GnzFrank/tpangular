<?php
session_start(); 
header("Access-Control-Allow-Origin: *"); 
header("Access-Control-Allow-Methods: PUT, GET, POST, DELETE");
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");

include('../db.php');
$sucursal = $_GET['sucursal'];
$mesDesde = $_GET['mesDesde'];
$mesHasta = $_GET['mesHasta'];

$sql = "SELECT SUM($sucursal) AS total_ventas
        FROM venta
        WHERE mes BETWEEN $mesDesde AND $mesHasta";

$vec = [];
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        $vec[] = $row;
    }
}
echo json_encode($vec);
$conn->close();
?>