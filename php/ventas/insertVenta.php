<?php
session_start(); 
header("Access-Control-Allow-Origin: *"); 
header("Access-Control-Allow-Methods: PUT, GET, POST, DELETE");
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");

include('../db.php');

$data = json_decode(file_get_contents("php://input"));
$mes = $data->mes;
$sucursal = $data->sucursal;
$monto = $data->monto;
$operacion = $data->operacion;

if ($operacion === "suma") {
    $sql = "UPDATE venta
        SET $sucursal = (SELECT $sucursal FROM venta WHERE mes = $mes) + $monto
        WHERE mes = $mes";
} elseif ($operacion === "resta") {
    $sql = "UPDATE venta
            SET $sucursal = (SELECT $sucursal FROM venta WHERE mes = $mes) - $monto
            WHERE mes = $mes";
}

if ($conn->query($sql) === TRUE) {
    echo json_encode(["message" => "Venta actualizada correctamente"]);
} else {
    echo json_encode(["error" => "Error al actualizar la venta: " . $conn->error]);
}

$conn->close();
?>