<?php
session_start(); 
header("Access-Control-Allow-Origin: *"); 
header("Access-Control-Allow-Methods: PUT, GET, POST, DELETE");
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");

include('../db.php');
$sql = "SELECT SUM(Suc1) AS 'Santa Fe', SUM(Suc2) AS 'Rosario', SUM(Suc3) AS 'Santo Tome', SUM(Suc4) AS 'Rafaela', SUM(Suc5) AS 'Parana'
        FROM venta";
$vec = [];
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        $vec[] = $row;
    }
}
echo json_encode($vec);
$conn->close();
?>