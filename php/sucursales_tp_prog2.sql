-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 09-10-2023 a las 16:29:11
-- Versión del servidor: 10.4.24-MariaDB
-- Versión de PHP: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `sucursales_tp_prog2`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articulos`
--

CREATE TABLE `articulos` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(50) DEFAULT NULL,
  `precio` float DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `articulos`
--

INSERT INTO `articulos` (`id`, `descripcion`, `precio`) VALUES
(1, 'papas fritas', 2300),
(2, 'Naranjas', 450),
(3, 'Manzanas', 100),
(4, 'Mandarin', 10),
(10, 'Prueba ', 120),
(19, 'Mandarinas', 1010);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE `clientes` (
  `id` int(11) NOT NULL,
  `apellido` varchar(50) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `saldo` int(11) NOT NULL,
  `status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`id`, `apellido`, `nombre`, `saldo`, `status`) VALUES
(1, 'asd', 'asd', 0, 0),
(2, 'Bianchi', 'Germán', 92000, 1),
(3, 'Costa', 'María Laura', 45000, 0),
(4, 'Donzelli', 'Nicolas Emanuel', 92000, 0),
(5, 'Giavedoni', 'Augusto', 82000, 1),
(6, 'Girod', 'Ignacio', 82000, 1),
(7, 'Imhoff', 'Marianela', 80000, 1),
(8, 'Kouefati', 'Jacques', 80000, 0),
(9, 'Pallavidini', 'Nahuel', 80000, 1),
(57, 'Donzelli', 'Nicolas Emanuel', 4, 0),
(58, 'Donzelli', 'Nicolas Emanuel', 4, 0),
(60, 'pepe', 'grillo', 666, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `meses`
--

CREATE TABLE `meses` (
  `Nro` int(2) NOT NULL,
  `Mes` varchar(50) NOT NULL,
  `CDias` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `meses`
--

INSERT INTO `meses` (`Nro`, `Mes`, `CDias`) VALUES
(1, 'Enero', 31),
(2, 'Febrero', 28),
(3, 'Marzo', 31),
(4, 'Abril', 30),
(5, 'Mayo', 31),
(6, 'Junio', 30),
(7, 'Julio', 31),
(8, 'Agosto', 31),
(9, 'Septiembre', 30),
(10, 'Octubre', 31),
(11, 'Noviembre', 30),
(12, 'Diciembre', 31);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sucursales`
--

CREATE TABLE `sucursales` (
  `Id_Suc` int(11) NOT NULL,
  `Nombre_Suc` varchar(50) NOT NULL,
  `Dir_Suc` varchar(50) NOT NULL,
  `Cant_Emp_Suc` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `sucursales`
--

INSERT INTO `sucursales` (`Id_Suc`, `Nombre_Suc`, `Dir_Suc`, `Cant_Emp_Suc`) VALUES
(1, 'Santa Fe', 'San  Martin 1111', 11),
(2, 'Rosario', 'Belgrano 2222', 22),
(3, 'Santo tome', '9 de Julio 3333', 16),
(4, 'Rafaela', 'Roca 4444', 4),
(5, 'Parana', 'Tunel 555', 15);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `venta`
--

CREATE TABLE `venta` (
  `mes` int(11) NOT NULL,
  `Suc1` float NOT NULL,
  `Suc2` float NOT NULL,
  `Suc3` float NOT NULL,
  `Suc4` float NOT NULL,
  `Suc5` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `venta`
--

INSERT INTO `venta` (`mes`, `Suc1`, `Suc2`, `Suc3`, `Suc4`, `Suc5`) VALUES
(1, 8386, 8679, 6853, 5111, 8019),
(2, 7024, 9289, 7299, 5612, 7721),
(3, 5329, 7019, 5203, 9400, 9291),
(4, 45718, 9151, 6667, 9836, 8054),
(5, 5154, 9487, 6084, 8365, 9942),
(6, 8528, 5101, 8775, 9921, 7077),
(7, 6603, 5962, 9845, 6703, 5561),
(8, 5881, 7857, 6640, 9669, 6169),
(9, 7952, 8372, 5312, 6248, 6714),
(10, 7249, 5031, 9476, 9434, 6759),
(11, 5393, 7977, 6925, 7619, 8966),
(12, 8366, 5008, 5193, 8093, 7039);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ventas`
--

CREATE TABLE `ventas` (
  `mes` int(11) NOT NULL,
  `id_suc` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `ventas`
--

INSERT INTO `ventas` (`mes`, `id_suc`, `cantidad`) VALUES
(1, 1, 8386),
(1, 2, 8679),
(1, 3, 6853),
(1, 4, 5111),
(1, 5, 8019),
(2, 1, 7024),
(2, 2, 9289),
(2, 3, 7299),
(2, 4, 5678),
(2, 5, 8432),
(3, 1, 7465),
(3, 2, 9765),
(3, 3, 4623),
(3, 4, 7489),
(3, 5, 6543),
(4, 1, 4758),
(4, 2, 9647),
(4, 3, 7342),
(4, 4, 13124),
(4, 5, 5677),
(5, 1, 8386),
(5, 2, 6879),
(5, 3, 8465),
(5, 4, 4255),
(5, 5, 5923),
(6, 1, 5746),
(6, 2, 8346),
(6, 3, 6788),
(6, 4, 2533),
(6, 5, 4356),
(7, 1, 3546),
(7, 2, 6898),
(7, 3, 7854),
(7, 4, 9067),
(7, 5, 4567),
(8, 1, 2347),
(8, 2, 8465),
(8, 3, 6342),
(8, 4, 2134),
(8, 5, 4846),
(9, 1, 5498),
(9, 2, 8746),
(9, 3, 6788),
(9, 4, 4533),
(9, 5, 6455),
(10, 1, 5253),
(10, 2, 18346),
(10, 3, 6474),
(10, 4, 2533),
(10, 5, 8653),
(11, 1, 6785),
(11, 2, 2346),
(11, 3, 9834),
(11, 4, 4756),
(11, 5, 2354),
(12, 1, 8356),
(12, 2, 7647),
(12, 3, 2576),
(12, 4, 11397),
(12, 5, 6456);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `articulos`
--
ALTER TABLE `articulos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `venta`
--
ALTER TABLE `venta`
  ADD PRIMARY KEY (`mes`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `articulos`
--
ALTER TABLE `articulos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT de la tabla `clientes`
--
ALTER TABLE `clientes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT de la tabla `venta`
--
ALTER TABLE `venta`
  MODIFY `mes` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
